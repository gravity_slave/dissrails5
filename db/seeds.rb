User.create!(
        email: 'admin@test.com',
        password: 'asdfasdf',
        password_confirmation: "asdfasdf",
        name: "DEUS VULT",
        roles: "site_admin"
)

puts "i created admin user"

User.create!(
    email: 'test@test.com',
    password: 'asdfasdf',
    password_confirmation: "asdfasdf",
    name: "DEUSVULT DEUS"

)


3.times do |topic|
  Topic.create(title: "deus vult #{topic}")
end

puts "3 topic creates"

10.times do |i|
  Blog.create!(title: "my blog pos #{i}", body: "Cras vel eros id mi sollicitudin tristique. Sed ornare odio nibh,
 egestas fermentum magna sodales at. Suspendisse potenti. In interdum convallis lacus,
 eget porta arcu scelerisque vel. Nam convallis iaculis magna, elementum commodo nulla iaculis in.
 Aenean hendrerit magna sed metus euismod fermentum. Aliquam vehicula scelerisque rhoncus. ",
               topic: Topic.last)
end
puts " 10 blog posts created"

5.times do |sk|
  Skill.create!(title: "Rails #{sk}", percent_utilized: sk*2)

end

puts "5 skills created"

8.times do |k|
  Portfolio.create!(title: "Portfolio title #{k}" ,subtitle: "DEUS VULT",
                    body: "Donec finibus posuere nisl in efficitur. Integer mi risus, volutpat nec justo id,
 porta commodo nulla. Fusce finibus id mauris bibendum congue. Praesent ut mi ac eros vehicula viverra.
 Suspendisse aliquam ligula ac lacinia viverra.
Morbi sodales aliquet risus, vel cursus erat. Nulla id augue at dui tincidunt tincidunt a tempus nunc. ",

    position: k+1)
end

1.times do |k|
  Portfolio.create!(title: "Portfolio title #{k+8}" ,subtitle: "AVE MARIA",
                    body: "Donec finibus posuere nisl in efficitur. Integer mi risus, volutpat nec justo id,
 porta commodo nulla. Fusce finibus id mauris bibendum congue. Praesent ut mi ac eros vehicula viverra.
 Suspendisse aliquam ligula ac lacinia viverra.
Morbi sodales aliquet risus, vel cursus erat. Nulla id augue at dui tincidunt tincidunt a tempus nunc. ",
                    main_image: "http://placehold.it/600x400",
                    thumb_image: "http://placehold.it/350X200",
    position: k+8)
end
puts "nine portfolio files created"

3.times do |tech|
  Portfolio.last.technologies.create!(name: "Technology #{tech}")
end

p "Technology created"
