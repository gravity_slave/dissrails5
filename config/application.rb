
require_relative 'boot'

require "rails"
# Pick the frameworks you want:
require "active_model/railtie"
require "active_job/railtie"
require "active_record/railtie"
require "action_controller/railtie"
require "action_mailer/railtie"
require "action_view/railtie"
require "action_cable/engine"
require "sprockets/railtie"
require  "dotenv-rails"
# require "rails/test_unit/railtie"

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module DevcampPortfolio
  class Application < Rails::Application
    config.eager_load_paths << "#{Rails.root}/lib"
    config.secret_key = 'c863fec5441c9a7b8c47399af682f6e31a74a9a6ffd0ef349008cb024294fd9fde7642a3e7605c60954f96849b198841f0d467e2e793cb814b3dbc07f36a9838'

  end
end
