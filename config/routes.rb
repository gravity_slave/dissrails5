Rails.application.routes.draw do

  resources :topics, only: [:index, :show]

  resources :comments, only: [:create]
  devise_for :users, path: '', path_names: { sign_in: 'login', sign_out: 'logout', sign_up: 'register' }
  resources :portfolios, except: [:show] do
    put :sort, on: :collection
  end
 get 'portfolio/:id', to: 'portfolios#show', as: 'portfolio_show'

  get 'contacts', to: 'pages#contacts'

  get 'about', to: 'pages#about'
  get 'pages/home'
  get 'twit-news', to: 'pages#twitter'
  get 'pages/about'

  get 'pages/contact'
  resources :blogs do
    member do
      get :toggle_status
    end
  end
  root to:  "pages#home"
mount ActionCable.server => '/cable'

end
