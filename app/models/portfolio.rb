class Portfolio < ApplicationRecord
  has_many :technologies
  accepts_nested_attributes_for :technologies,
                                allow_destroy: true,
                                reject_if: lambda  {|attr| attr["name"].blank? }
  validates_presence_of :title, :body
 scope :by_position, -> { order(position: :asc)}

  mount_uploader :thumb_image, PortfolioUploader
  mount_uploader :main_image, PortfolioUploader

def  self.deus_vult
  where(subtitle:  'DEUS VULT')
end

  scope :ave_maria, -> { where(subtitle: "AVE MARIA")}



end



