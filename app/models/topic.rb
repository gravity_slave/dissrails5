class Topic < ApplicationRecord
  validates :title, presence: true
  has_many :blogs

  scope :with_blogs, lambda { includes(:blogs).where.not( blogs: { id: nil} ) }
end
