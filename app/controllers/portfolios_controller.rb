class PortfoliosController < ApplicationController
  layout 'portfolio'

  before_action :set_portfolio, only: [:show, :edit, :update, :destroy]
  access all: [:show, :index], user: {except: [:destroy, :new, :create, :update, :sort ,:edit]}, site_admin: :all
  def index
    @portfolios = Portfolio.by_position

  end

  def sort
    params[:order].each do |key, value|
      Portfolio.find(value[:id]).update(position: value[:position])
    end

    render nothing: true
  end
  def new
    @portfolio = Portfolio.new


  end
  def show

  end

  def edit

  end

  def update
     @portfolio.update_attributes(portfolio_params)
      if @portfolio.save
        redirect_to portfolio_show_path(@portfolio), notice: "This portfolio was Successcully updated"
      else
        render 'edit'
      end
  end
  def create
    @portfolio = Portfolio.new(portfolio_params)
    if @portfolio.save
      redirect_to portfolio_show_path(@portfolio), notice: "Portfolio has been created"
    else
      render 'new'
    end
  end

  def destroy
    if @portfolio.destroy
      redirect_to portfolios_path, alert: 'Portfolio was successfully deleted'
    end
  end

  private

  def portfolio_params
    params.require(:portfolio).permit(:title, :subtitle, :body, :main_image, :thumb_image,
                                      technologies_attributes: [:id,:name, :_destroy])
  end

  def set_portfolio
    @portfolio = Portfolio.find(params[:id])
  end
end
