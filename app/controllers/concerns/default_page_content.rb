module DefaultPageContent
  extend ActiveSupport::Concern
  included do
    before_filter :set_title
  end

  def set_title
    @page_title = "Deus Vult | Non Nobis Domine"
    @seo_keywords = "Saracen occupation of the Holy Land"
  end
end