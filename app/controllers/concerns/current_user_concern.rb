module CurrentUserConcern
  extend ActiveSupport::Concern

  def current_user
    super || guest_user
  end

  def guest_user
    guest = GuestUser.new
    guest.name = "Deus Vult"
    guest.first_name = "Deus"
    guest.last_name = "Vult"
    guest.email = "wewill@take.jerusakem"
    guest

  end
end