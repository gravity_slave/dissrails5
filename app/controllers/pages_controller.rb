class PagesController < ApplicationController
  def home
    @posts = Blog.all
    @skills = Skill.all
  end

  def contacts
  end

  def about
    @skills =Skill.all
  end

  def twitter
    @tweets = SocialTool.twitter_search
  end


  def contact
  end
end
