module ApplicationHelper

  def login_helper style
    unless current_user.is_a?(GuestUser)
       (link_to 'Logout', destroy_user_session_path, method: :delete, class:  style) +
       " ".html_safe +
       (link_to 'Edit', edit_user_registration_path, class:  style)
    else
       (link_to "Login", new_user_session_path, class: style) +
       " ".html_safe +
       (link_to "Register", new_user_registration_path, class: style)
    end

  end

  def sample_helper(styles)
    if session[:source]
     greeting = "Thanks for visiting me from #{session[:source]}, please fill free to #{ link_to 'Contact me', contacts_path},
    if you like to crusade together "
      content_tag(:p, greeting.html_safe, class: styles )

  end
  end

  def copyright_generator
    DevcampViewTool::Renderer.copyright 'graviyslave92', "DEUS VULT INFIDEL"
  end

  def nav_items
    [
        {
            url: root_path,
            title: 'Home'
        },
        {
            url: about_path,
            title: 'Bio'
        },
        {
            url: contacts_path,
            title: 'Contact'
        },
        {
            url: blogs_path,
            title: 'Blog'
        },
        {
            url: portfolios_path,
            title: 'Portfolio'
        },
        {
            url: twit_news_path,
            title: 'Tweets'
        },

    ]
  end

  def nav_helper style, tag_type
    nav_links = ''

    nav_items.each do |item|
      nav_links << "<#{tag_type}><a href='#{item[:url]}' class='#{style} #{active? item[:url]}'>#{item[:title]}</a></#{tag_type}>"
    end

    nav_links.html_safe
  end

  def active? path
    "active" if current_page? path
  end



  def alerts
    alert = ( flash[:alert] || flash[:error] || flash[:notice])

    if alert
      alert_generator alert
    end
  end

  def alert_generator msg
    js add_gritter(msg, title: 'Please, read this message', sticky: false)
  end
end


