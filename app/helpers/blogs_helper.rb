module BlogsHelper
  def gravatar_helper user
    image_tag "https://www.gravatar.com/avatar/#{Digest::MD5.hexdigest(user.email)}", width: 60
  end

  class CodeRayify < Redcarpet::Render::HTML
    def block_code(code, lang)
      CodeRay.scan(code, lang).div

    end
  end

def markdown(text)
  coderayified = CodeRayify.new(filter_html: true,hard_wrap: true)

  options = {
      fenced_code_blocks: true,
      no_intra_emphasis: true,
      lax_html_blocks: true,
      autolink: true,

  }

  markdown_to_html = Redcarpet::Markdown.new( coderayified, options)
  markdown_to_html.render(text).html_safe

end

  def blog_status_color item
    if item.draft?
      'color: red;'
    else
      'color: blue;'
    end
  end


end
